from pathlib import Path

from pypdf import PdfReader, PdfWriter

INPUT_PDF = Path("example.pdf")
OUTPUT_PDF = Path("scaled-example.pdf")

SCALE_FACTOR = 4

if __name__ == "__main__":
    reader = PdfReader(INPUT_PDF, strict=True)
    writer = PdfWriter()

    for page_number, page in enumerate(reader.pages, start=1):
        page.scale_by(SCALE_FACTOR)
        writer.add_page(page)
        print(f"Page {page_number} ✔")

    with open(OUTPUT_PDF, "wb") as f:
        writer.write(f)

    print("Done!")
