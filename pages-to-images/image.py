from pathlib import Path

import fitz

INPUT_PDF = Path("scaled-example.pdf")
# INPUT_PDF = Path("example.pdf")

if __name__ == "__main__":
    doc = fitz.open(INPUT_PDF)
    # print(doc)

    for page_number, page in enumerate(doc, start=1):
        pix = page.get_pixmap(dpi=300)
        # print(pix)
        # print(pix.xres, pix.yres)

        pix.save(f"page_{page_number}.png")
        print(f"Page {page_number} ✔")

    print("Done!")
