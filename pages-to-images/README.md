# pages-to-images

## Development

```bash
conda env create -f environment.yml
```

```bash
conda activate pages-to-images
```

```bash
python scale.py
```

```bash
python image.py
```

```bash
black *.py
```
