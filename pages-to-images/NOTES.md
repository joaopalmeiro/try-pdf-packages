# Notes

- https://pymupdf.readthedocs.io/en/latest/pixmap.html#Pixmap.save
- https://pymupdf.readthedocs.io/en/latest/pixmap.html#pixmapoutput
- https://pymupdf.readthedocs.io/en/latest/page.html#Page.get_pixmap
- https://github.com/sk1project/uniconvertor
- https://pymupdf.readthedocs.io/en/latest/glossary.html#resolution
- https://pymupdf.readthedocs.io/en/latest/pixmap.html#Pixmap.set_dpi
- https://pypi.org/project/PyMuPDF/
- https://pymupdf.readthedocs.io/en/latest/
- https://pymupdf.readthedocs.io/en/latest/pixmap.html
- https://pypi.org/project/pikepdf/
- https://pypi.org/project/pdfplumber/
- https://pypdf.readthedocs.io/en/latest/meta/comparisons.html
- https://stackoverflow.com/questions/6536552/resize-pdf-pages-in-python
- https://pypdf.readthedocs.io/en/latest/user/installation.html
- https://github.com/py-pdf/pypdf
- https://pypdf.readthedocs.io/en/latest/modules/PdfReader.html
- https://pypdf.readthedocs.io/en/latest/user/cropping-and-transforming.html#scaling-a-page-the-canvas
- https://pypdf.readthedocs.io/en/latest/user/metadata.html
- https://pypdf2.readthedocs.io/en/latest/user/file-size.html
- https://en.wikipedia.org/wiki/Check_mark
- https://stackoverflow.com/questions/69643954/converting-pdf-to-png-with-python-without-pdf2image
- https://github.com/python/cpython/blob/v3.10.11/Lib/ensurepip/__init__.py#L15 (23.0.1)
- https://github.com/python/cpython/blob/main/Lib/ensurepip/__init__.py
- https://docs.python.org/3.10/library/ensurepip.html
- https://pip.pypa.io/en/stable/installation/#ensurepip
- `python -c 'import ensurepip; print(ensurepip.version())'` (23.0.1)
- https://github.com/github/gitignore/blob/main/Global/macOS.gitignore

## Commands

```bash
conda deactivate && conda env remove --name pages-to-images
```

```bash
rm -rf *.pdf *.png
```
