# Notes

- https://github.com/joaopalmeiro/template-python-uv-script
- https://pypdf.readthedocs.io/en/stable/user/installation.html
- https://pypi.org/project/pypdf/
- https://pypdf.readthedocs.io/en/stable/user/extract-images.html
- https://python.langchain.com/v0.1/docs/modules/data_connection/document_loaders/pdf/
- https://github.com/RapidAI/RapidOCR
- https://pdfminersix.readthedocs.io/en/latest/reference/highlevel.html
- https://github.com/pdfminer/pdfminer.six/blob/20231228/tools/pdf2txt.py#L62
- https://github.com/pdfminer/pdfminer.six/issues/847
- https://loguru.readthedocs.io/en/stable/api/type_hints.html
- https://github.com/kornicameister/loguru-mypy
- https://stackoverflow.com/questions/2189800/how-to-find-length-of-digits-in-an-integer
- https://stackoverflow.com/questions/39402795/how-to-pad-a-string-with-leading-zeros-in-python-3
- https://discuss.python.org/t/a-function-for-calculating-magnitude-order-of-a-number/18924/26
- https://stackoverflow.com/questions/16839190/python-categorising-a-list-by-orders-of-magnitude
- https://gist.github.com/gauthamchettiar/fa3f32758616611bfecf941809fd598b

## Commands

```bash
pip config unset global.require-virtualenv
```

```bash
rm -rf .venv/ .mypy_cache/ .ruff_cache/
```

```bash
deactivate && uv venv .venv && source .venv/bin/activate && uv pip install -r requirements.txt
```

```bash
uv venv .venv && source .venv/bin/activate && uv pip install -r requirements.txt
```
