from pathlib import Path

from gaveta.files import ensure_folder
from loguru import logger
from pypdf import PdfReader

INPUT_PDF = Path("example.pdf")
OUTPUT_FOLDER = Path("images")

if __name__ == "__main__":
    ensure_folder(OUTPUT_FOLDER)

    reader = PdfReader(INPUT_PDF)

    for index_page, page in enumerate(reader.pages, start=1):
        for index_image, image_file_object in enumerate(page.images, start=1):
            output_image = (
                OUTPUT_FOLDER / f"{index_page}-{index_image}-{image_file_object.name}"
            )

            with output_image.open(mode="wb") as fp:
                fp.write(image_file_object.data)

            logger.info("{} ✓", output_image)
        logger.info("Page {} ✓", index_page)
